import { MaterialDesignAppPage } from './app.po';

describe('material-design-app App', function() {
  let page: MaterialDesignAppPage;

  beforeEach(() => {
    page = new MaterialDesignAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
