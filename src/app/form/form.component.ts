import { Component } from '@angular/core';
import { ChipsComponent } from '../chips/chips.component';

@Component({
    selector: 'app-form',
    templateUrl: 'form.component.html',
    styleUrls: ['../app.component.css', 'form.component.css'],
    providers: [ChipsComponent]
})
export class FormComponent {

  chips = ChipsComponent;
  saveData() {
    console.log('Data saved: ', this.chips);
  }

}
