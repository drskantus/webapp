import { Component } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
    providers: []
})

export class AppComponent {

  formShowing: boolean = false;
  views: Object[] = [
    {
      name: 'My Account',
      description: 'Edit my account information',
      icon: 'assignment ind'
    },
    {
      name: 'Potential dates',
      description: 'Find your soulmate!',
      icon: 'pets'
    }
  ];

}
