/* Modules */
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import 'hammerjs';

/* Components */
import { AppComponent } from './app.component';
import { FormComponent } from './form/form.component';
import { CardComponent } from './card/card.component';
import { ChipsComponent } from './chips/chips.component';
import { ConfirmDialog }   from './dialog/confirm-dialog.component';

/* Services */
import { AppService } from './app.service';
import { DialogsService } from './dialog/dialog.service';

/* @NgModule */
@NgModule({
  declarations: [
    AppComponent,
    FormComponent,
    CardComponent,
    ChipsComponent,
    ConfirmDialog
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule.forRoot()
  ],
  exports: [
    ConfirmDialog,
  ],
  schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ],
  providers: [
    AppService,
    DialogsService
  ],
  entryComponents: [
    ConfirmDialog,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
