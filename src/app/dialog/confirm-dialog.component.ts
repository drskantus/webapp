import { MdDialogRef } from '@angular/material';
import { Component } from '@angular/core';

let dialog = `
  <md-content class="md-padding" layout-xs="column" layout="row">
    <h3>{{ title }} </h3>
    <p>{{ message }}</p>
    <button type="button" md-raised-button (click)="dialogRef.close(true)">OK</button>
    <button type="button" md-button (click)="dialogRef.close()">Cancel</button>
  </md-content>
`;

@Component({
    selector: 'confirm-dialog',
    template: dialog,
})
export class ConfirmDialog {

    public title: string;
    public message: string;

    constructor(public dialogRef: MdDialogRef<ConfirmDialog>) {

    }
}
