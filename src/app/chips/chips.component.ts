import { Component, Input } from '@angular/core';
import {MdInput} from '@angular/material';

export interface Person {
  name: string;
}

@Component({
    selector: 'app-chips',
    templateUrl: 'chips.component.html',
    styleUrls: ['chips.component.css'],
    providers: []
})

export class ChipsComponent {

  @Input() people: Person[] = [
    { name: 'Alejo Castaño' },
    { name: 'Jeremy' },
    { name: 'Topher' }
  ];

  // Add new chip.
  add(input: MdInput): void {
    if (input.value && input.value.trim() !== '') {
      this.people.push({ name: input.value.trim() });
      input.value = '';
    }
  }

}
