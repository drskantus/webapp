import { Component, ViewContainerRef } from '@angular/core';
import { Http } from '@angular/http';
import { DialogsService } from '../dialog/dialog.service';

@Component({
    selector: 'app-card',
    templateUrl: 'card.component.html',
    styleUrls: ['card.component.css'],
    providers: [
      DialogsService
    ]
})

export class CardComponent {

  public result: any;

  dogs: Object[];
  constructor(public  http: Http,
              private dialogsService: DialogsService,
              private viewContainerRef: ViewContainerRef) {
    // CRUD
    http.get('./api/dogs.json')
      .map(res => res.json())
        .subscribe((res) => this.dogs = res);
  }

  public openDialog(dog) {
    this.dialogsService
      .confirm('Hi, my name is ' + dog.name, 'Do you love me?', this.viewContainerRef)
      .subscribe(res => this.result = res);
  }

}
